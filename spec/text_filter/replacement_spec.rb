require 'rails_helper'

describe TextFilter::Replacement do
  before(:all) do
    @replacement = TextFilter::Replacement.new
    @test_text = 'text **foo** text \\\\bar\\\\ ((xxx/yyy zzz))'
  end

  it '#replace' do
    expect(@replacement.replace(@test_text)).to eq("text <b>foo</b> text <i>bar</i> <a href='/xxx/yyy'>zzz</a>")
  end
end
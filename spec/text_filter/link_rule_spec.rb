require 'rails_helper'

describe TextFilter::LinkRule do
  before(:all) do
    @rule = TextFilter::LinkRule.new
    @test_text = 'test text ((xxx/yyy/zzz hello)) text text'
  end

  it '#pattern' do
    expect(@test_text).to match(/#{@rule.pattern}/)
  end

  it '#apply' do
    @test_text =~ /#{@rule.pattern}/
    expect(@rule.apply(Regexp.last_match)).to eq("<a href='/xxx/yyy/zzz'>hello</a>")
  end
end
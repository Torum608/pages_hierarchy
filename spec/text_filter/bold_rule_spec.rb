require 'rails_helper'

describe TextFilter::BoldRule do
  before(:all) do
    @rule = TextFilter::BoldRule.new
    @test_text = 'test text **foo bar** text text'
  end

  it '#pattern' do
    expect(@test_text).to match(/#{@rule.pattern}/)
  end

  it '#apply' do
    @test_text =~ /#{@rule.pattern}/
    expect(@rule.apply(Regexp.last_match)).to eq('<b>foo bar</b>')
  end
end
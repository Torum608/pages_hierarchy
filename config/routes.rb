Rails.application.routes.draw do
  root 'pages#index'

  controller :pages do
    get '', action: :index, as: :pages
    get '(*ids)/add', action: :new, as: :new_page
    get '*ids/edit', action: :edit, as: :edit_page
    get '(*ids)', action: :show, as: :page
    post '(*ids)', action: :create
    put '*ids', action: :update
    patch '(*ids)', action: :update
  end
end
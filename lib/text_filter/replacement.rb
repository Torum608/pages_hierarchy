module TextFilter

  # Replacement uses predetermined rules for replacement text.
  #
  # Options are:
  #   :rule_types - custom rules
  #
  # Example:
  #  r = TextFilter::Replacement.new
  #  r.replace('source text')
  #
  class Replacement
    attr_reader :regexp, :rules

    RULE_TYPES = [BoldRule, ItalicRule, LinkRule]

    def initialize(options = {})
      @rules = build_rules(options[:rule_types] || RULE_TYPES)
      @regexp = build_regexp(@rules)
    end

    def replace(text)
      text.gsub(regexp) do
        rule = detect_rule_by_match_data(Regexp.last_match)
        rule.apply(Regexp.last_match)
      end
    end

    private

    def build_rules(rule_types)
      rule_types.map(&:new)
    end

    def build_regexp(rules)
      /#{rules.map(&:pattern).join('|')}/m
    end

    def detect_rule_by_match_data(match_data)
      rules.detect { |rule| !!match_data[rule.name] }
    end
  end
end
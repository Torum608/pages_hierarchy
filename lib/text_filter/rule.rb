module TextFilter

  # Abstract rule for replacement text.
  # You must override:
  #   definition - pattern for search in the text.
  #   replacement - text to replace. Will be transferred named groups mentioned in the definition.
  #
  class Rule
    attr_reader :name, :pattern

    def initialize
      @name = generate_name
      @pattern = build_pattern(@name)
    end

    def apply(match_data)
      groups = collect_groups(match_data)
      replacement(groups)
    end

    def definition
    end

    private

    def replacement(groups = {})
    end

    def generate_name
      self.class.to_s.parameterize('_').to_sym
    end

    def build_pattern(name)
      "(?<#{name}>#{definition})"
    end

    def collect_groups(match_data)
      match_data.names.map{ |name| [name.to_sym, match_data[name]] }.to_h.compact
    end
  end
end
module TextFilter

  class BoldRule < Rule

    def definition
      '\*{2}(?<text>.*?)\*{2}'
    end

    private

    def replacement(groups = {})
      "<b>#{groups[:text]}</b>"
    end
  end
end
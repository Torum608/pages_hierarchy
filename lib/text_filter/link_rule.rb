module TextFilter

  class LinkRule < Rule

    def definition
      '\({2}\W*(?<path>(\w+\/?)+)\W+?(?<text>.*?)\){2}'
    end

    private

    def replacement(groups = {})
      "<a href='/#{groups[:path]}'>#{groups[:text]}</a>"
    end
  end
end
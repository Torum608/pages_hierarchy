module TextFilter

  class ItalicRule < Rule

    def definition
      '\\\\{2}(?<text>.*?)\\\\{2}'
    end

    private

    def replacement(groups = {})
      "<i>#{groups[:text]}</i>"
    end
  end
end
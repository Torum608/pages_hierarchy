module LastUpdateDate

  def last_update_date
    select(:updated_at).order(:updated_at).last.try(:updated_at)
  end
end
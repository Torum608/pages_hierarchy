class Page < ActiveRecord::Base
  extend LastUpdateDate

  acts_as_nested_set touch: true

  validates :title,
            :slug,
            :source_text, presence: true
  validates :slug, not_reserved_route: true, format: { with: /\A[а-яА-Яa-zA-Z0-9_]+\z/ }, uniqueness: true

  before_save :filter_source_text, if: Proc.new { |page| page.source_text_changed? }

  def to_param
    slug
  end

  private

  def filter_source_text
    self.text = TextFilter::Replacement.new.replace(source_text)
  end
end
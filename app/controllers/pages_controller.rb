class PagesController < ApplicationController
  before_filter :load_parent_page, only: [:new, :create]
  before_filter :load_page, only: [:show, :edit, :update]

  def new
    @page = @parent_page ? @parent_page.children.build : Page.new
  end

  def create
    @page = @parent_page ? @parent_page.children.build(page_create_params) : Page.new(page_create_params)
    if @page.save
      redirect_to page_path(ids: @page.self_and_ancestors.to_a)
    else
      render :new
    end
  end

  def update
    if @page.update_attributes(page_update_params)
      redirect_to page_path(ids: @page.self_and_ancestors.to_a)
    else
      render :edit
    end
  end

  private

  def page_create_params
    params.require(:page).permit(:title, :slug, :source_text)
  end

  def page_update_params
    params.require(:page).permit(:title, :source_text)
  end

  def load_parent_page
    @parent_page = load_nested_page(params[:ids])
  end

  def load_page
    @page = load_nested_page(params[:ids])
  end

  # Finds the page from slugs path.
  def load_nested_page(path)
    return unless path.present?

    slugs = path.split('/')
    page = Page.find_by!(slug: slugs.last)

    # Validation slugs path
    raise ActiveRecord::RecordNotFound, "Couldn't find Page: wrong ancestors" unless page.self_and_ancestors.map(&:slug) == slugs

    page
  end
end
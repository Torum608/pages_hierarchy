module PagesHelper

  # Iterates over descendants elements with the current stack in the tree.
  # Stack includes current element and ancestors elements.
  # Also indicates level shift from the root element.
  # Example:
  #    each_nested_page_with_stack(Page.first) do |page, stack, shift|
  #
  def each_nested_page_with_stack(root_page = nil, &block)
    pages, stack = if root_page
                     [root_page.descendants, root_page.self_and_ancestors]
                   else
                     [Page.order(:lft), []]
                   end

    initial_depth = stack.length
    pages.each do |page|
      stack = stack.slice(0, page.depth)
      stack.push(page)
      block.call(page, stack, page.depth - initial_depth)
    end
  end
end
class NotReservedRouteValidator < ActiveModel::EachValidator

  RESERVED_WORDS = %w(new add edit assets)

  def validate_each(record, attribute, value)
    if RESERVED_WORDS.include?(value)
      record.errors[attribute] << (options[:message] || I18n.t('errors.messages.not_reserved_route'))
    end
  end
end
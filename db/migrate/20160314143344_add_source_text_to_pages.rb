class AddSourceTextToPages < ActiveRecord::Migration
  def change
    add_column :pages, :source_text, :text
  end
end